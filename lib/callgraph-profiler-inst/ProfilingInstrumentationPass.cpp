

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"
#include <vector>
#include "ProfilingInstrumentationPass.h"


using namespace cgprofiler;
using namespace llvm;

namespace cgprofiler {

char ProfilingInstrumentationPass::ID = 0;

} // namespace cgprofiler

static llvm::Constant*
createConstantString(llvm::Module& m, llvm::StringRef str) {
  auto& context = m.getContext();

  auto* name    = llvm::ConstantDataArray::getString(context, str, true);
  auto* int8Ty  = llvm::Type::getInt8Ty(context);
  auto* arrayTy = llvm::ArrayType::get(int8Ty, str.size() + 1);
  auto* asStr   = new llvm::GlobalVariable(
      m, arrayTy, true, llvm::GlobalValue::PrivateLinkage, name);

  auto* zero = llvm::ConstantInt::get(llvm::Type::getInt32Ty(context), 0);
  llvm::Value* indices[] = {zero, zero};
  return llvm::ConstantExpr::getInBoundsGetElementPtr(arrayTy, asStr, indices);
}

enum InstType {
	GetChar, Predicate, MethodCall, Other, Store, Load
};

struct statement
{
  //unsigned line;
  llvm::Instruction* instruction;
  InstType type;
  uint64_t id; //TODO
  // operands? //TODO

}; 


bool
ProfilingInstrumentationPass::runOnModule(llvm::Module& m) {

	std::vector<statement> statements;

    //unsigned line = 0;
    int counter = 0;
    //Go through each instruction
    for (auto& f : m) {
      for (auto& bb : f) {
        for (auto& i : bb) {

          //Get the line number of the instruction	 
          //if (DILocation *loc = i.getDebugLoc()) {	//Make sure the instruction is valid
           // line = loc->getLine();
           // outs() << line;
 
            statement s;	
            //s.line = line;
            s.instruction = &i;
            if (auto* AI = dyn_cast<CallInst>(&i)) {	//Is call, possibly a read
              auto* c = AI->getCalledFunction();
              if (c->getName().equals("fgetc")) {	//Is fgetc     
                s.type = InstType::GetChar;
                //outs() << " GetChar\n";

              } else if(!c->isDeclaration()) {
              	s.type = InstType::MethodCall;
              	for (auto& arg : AI->arg_operands()) {
              		auto* inst = dyn_cast<Instruction>(arg);
              		if (inst) {
              			outs() << "Argument:\n" << instructionIds[inst] << "\n--------\n";
              		}
              	}
              	//outs() << " MethodCall\n";
              } else {
              	s.type = InstType::Other;
              	//outs() << " Other\n";
              }

            } else if (isa<CmpInst>(&i)) {	//Is a predicate
              s.type = InstType::Predicate;
              //outs() << " Predicate\n";

   			} else if(isa<StoreInst>(&i)) {
   				s.type = InstType::Store;
   				//outs() << " Store\n";

   			} else if(isa<LoadInst>(&i)) {
   				s.type = InstType::Load;
   				//outs() << " Load\n";

            } else {
            	s.type = InstType::Other;
            	//outs() << " Other\n";
            }

            statements.push_back(s);
            s.id = counter;
            instructionIds[&i] = counter;
            counter++;
          }
      }
    }
	
  return true;
}