
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <fstream>


extern "C" {


// This macro allows us to prefix strings so that they are less likely to
// conflict with existing symbol names in the examined programs.
// e.g. CGPROF(entry) yields CaLlPrOfIlEr_entry
#define CGPROF(X) CaLlPrOfIlEr_##X

// The count of the number of unique caller-callee relationships is stored in a global variable 
// inside the instrumented module.
// eg. foo() is called by both bar() and bat(), thus is counted twice.
extern uint64_t CGPROF(callerCalleePairs);

extern uint64_t CGPROF(countSize);

// Has the callsite been updated since a count has been added?
// A check to prevent double counting with defined functions
bool CGPROF(callsiteUpdated) = false;

// ID of the callsite. Used to store the callsiteID globally for indirectly called functions
uint64_t CGPROF(callsiteID) = -1;

extern struct {
	uint64_t callerID;
	uint64_t targetID;
	char* caller;
	char* callee;	
	uint64_t lineNum;
	char* fileName;
	uint64_t count;
} CGPROF(callerInfo)[];

void 
CGPROF(called)(uint64_t tID) {
	if (CGPROF(callsiteUpdated)) {
		
		for(uint64_t i = 0; i < CGPROF(callerCalleePairs); i++) {
			auto& info = CGPROF(callerInfo)[i];
			if(info.callerID == CGPROF(callsiteID) && info.targetID == tID) {
				++CGPROF(callerInfo)[i].count;
			}
		}
	}	
	CGPROF(callsiteUpdated) = false;
}

void
CGPROF(updateCallsite)(uint64_t csId) {
	CGPROF(callsiteID) = csId;
	CGPROF(callsiteUpdated) = true;
}

void CGPROF(generateCSV)() {
	std::ofstream myfile;
	myfile.open ("profile-results.csv");
	for (size_t id = 0; id < CGPROF(callerCalleePairs); ++id) {
		auto& info = CGPROF(callerInfo)[id];
		if(info.count > 0) myfile << info.caller << ", " << info.fileName << ", " << info.lineNum << ", " << 
		info.callee << ", " << info.count << "\n";
	}
	myfile.close();
}

}
